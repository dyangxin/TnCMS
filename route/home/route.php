<?php

use think\facade\Route;

Route::get('p/:id','Post/detail');
Route::get('news/cate-:id','Post/postCate');
Route::get('news','Post/postCate');