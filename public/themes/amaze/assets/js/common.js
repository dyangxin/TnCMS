function getMyDate(str){
  str = str*1000;
  var timestamp = (new Date()).getTime();
  var timeDifference = timestamp - str;
  
  if(timeDifference > 604800000){
    var oDate = new Date(str),  
        oYear = oDate.getFullYear(),  
        oMonth = oDate.getMonth()+1,  
        oDay = oDate.getDate(),  
        oHour = oDate.getHours(),  
        oMin = oDate.getMinutes(),  
        oSen = oDate.getSeconds(),  
        oTime = oYear +'年'+ getzf(oMonth) +'月'+ getzf(oDay) + '日';//最后拼接时间  
        return oTime;  
  }else{
    timeDifference = parseInt(timeDifference/1000);
    if(timeDifference < 3600){
  		return parseInt(timeDifference/60) + '分钟前';
    }
     if(3600 <= timeDifference && timeDifference < 86400){
  		return  parseInt(timeDifference/3600) + '小时前';
    }
      if(86400 <= timeDifference && timeDifference < 604800){
      	return  parseInt(timeDifference/86400) + '天前';
      }
  }
}; 
//补0操作
function getzf(num){  
  if(parseInt(num) < 10){  
    num = '0'+num;  
  }  
  return num;  
}