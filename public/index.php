<?php

// [ 应用入口文件 ]
namespace think;

require __DIR__ . '/../vendor/autoload.php';

// 检测程序安装
if(!is_file(__DIR__ . '/install/install.lock')){
    header('Location: ./install/index.php');
    exit;
}

//定义系统版本
require __DIR__ . '/version.php';

// 执行HTTP应用并响应
$http = (new App())->http;

$response = $http->run();

$response->send();

$http->end($response);
