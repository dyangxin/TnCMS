<?php
/**
 * 安装向导
 */
header('Content-type:text/html;charset=utf-8');
// 检测是否安装过
if (file_exists('./install.lock')) {
    echo '你已经安装过该系统，请删除./install/文件';
    die;
}
// 同意协议页面
if (@!isset($_GET['c']) || @$_GET['c'] == 'agreement') {
    require './agreement.html';
}
// 检测环境页面
if (@$_GET['c'] == 'test') {
    require './test.html';
}
// 创建数据库页面
if (@$_GET['c'] == 'create') {
    require './create.html';
}
// 安装成功页面
if (@$_GET['c'] == 'success') {

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {

        $data = $_POST;
        $link = @new mysqli($data['DB_HOST'], $data['DB_USER'], $data['DB_PWD']);
        // 获取错误信息
        $error = $link->connect_error;
        if (!is_null($error)) {
            // 转义防止和alert中的引号冲突
            $error = addslashes($error);
            die("<script>alert('数据库链接失败:$error');history.go(-1)</script>");
        }

        $link->query("SET NAMES 'utf8'");
        $link->server_info > 5.0 or die("<script>alert('请将您的mysql升级到5.0以上');history.go(-1)</script>");
        // 创建数据库并选中
        if (!$link->select_db($data['DB_NAME'])) {
            $create_sql = 'CREATE DATABASE IF NOT EXISTS ' . $data['DB_NAME'] . ' DEFAULT CHARACTER SET utf8;';
            $link->query($create_sql) or die('创建数据库失败');
            $link->select_db($data['DB_NAME']);
        }
        // 导入sql数据并创建表
        $shujuku_str = file_get_contents('./TnCMS.sql');
        $sql_array = preg_split("/;[\r\n]+/", str_replace('teen_', $data['DB_PREFIX'], $shujuku_str));
        foreach ($sql_array as $k => $v) {
            if (!empty($v)) {
                $link->query($v);
            }
        }

        $db_str = <<<php
<?php

use think \ facade\Env;

return [
    // 默认使用的数据库连接配置
    'default'         => Env::get('database.driver', 'mysql'),

    // 自定义时间查询规则
    'time_query_rule' => [],

    // 自动写入时间戳字段
    // true为自动识别类型 false关闭
    // 字符串则明确指定时间字段类型 支持 int timestamp datetime date
    'auto_timestamp'  => true,

    // 时间字段取出后的默认时间格式
    'datetime_format' => 'Y年m月d日',

    // 数据库连接配置信息
    'connections'     => [
        'mysql' => [
            // 数据库类型
            'type'              => Env::get('database.type', 'mysql'),
            // 服务器地址
            'hostname'          => Env::get('database.hostname', '{$data['DB_HOST']}'),
            // 数据库名
            'database'          => Env::get('database.database', '{$data['DB_NAME']}'),
            // 用户名
            'username'          => Env::get('database.username', '{$data['DB_USER']}'),
            // 密码
            'password'          => Env::get('database.password', '{$data['DB_PWD']}'),
            // 端口
            'hostport'          => Env::get('database.hostport', '{$data['DB_PORT']}'),
            // 数据库连接参数
            'params'            => [],
            // 数据库编码默认采用utf8
            'charset'           => Env::get('database.charset', 'utf8'),
            // 数据库表前缀
            'prefix'            => Env::get('database.prefix', '{$data['DB_PREFIX']}'),
            // 数据库调试模式
            'debug'             => Env::get('database.debug', true),
            // 数据库部署方式:0 集中式(单一服务器),1 分布式(主从服务器)
            'deploy'            => 0,
            // 数据库读写是否分离 主从式有效
            'rw_separate'       => false,
            // 读写分离后 主服务器数量
            'master_num'        => 1,
            // 指定从服务器序号
            'slave_no'          => '',
            // 是否严格检查字段是否存在
            'fields_strict'     => true,
            // 是否需要断线重连
            'break_reconnect'   => false,
            // 字段缓存路径
            'schema_cache_path' => app()->getRuntimePath() . 'schema' . DIRECTORY_SEPARATOR,
        ],

        // 更多的数据库配置信息
    ],
];

php;
        // 创建数据库链接配置文件

        $fp = fopen('../../config/database.php', "w");

        fputs($fp, $db_str);

        fclose($fp);

        @touch('./install.lock');
        require './success.html';
    }

}
