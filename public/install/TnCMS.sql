/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : www.a.com

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 28/11/2019 11:39:46
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tn_category
-- ----------------------------
DROP TABLE IF EXISTS `tn_category`;
CREATE TABLE `tn_category`  (
  `cate_id` int(11) NOT NULL AUTO_INCREMENT,
  `cate_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `cover_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`cate_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tn_category
-- ----------------------------
INSERT INTO `tn_category` VALUES (2, '爱情', 'https://young.tn721.cn/uploads/cateCover/20191119/acbf2421af4aa3f3e5680f8f0520293d.jpg');
INSERT INTO `tn_category` VALUES (3, '生活', NULL);
INSERT INTO `tn_category` VALUES (4, '斜杆青年', NULL);
INSERT INTO `tn_category` VALUES (5, '励志鸡汤', NULL);
INSERT INTO `tn_category` VALUES (6, '青春', NULL);
INSERT INTO `tn_category` VALUES (7, '想法', NULL);

-- ----------------------------
-- Table structure for tn_comment
-- ----------------------------
DROP TABLE IF EXISTS `tn_comment`;
CREATE TABLE `tn_comment`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` int(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_comment
-- ----------------------------
INSERT INTO `tn_comment` VALUES (1, 26, 55, 'asd', 1574234227);
INSERT INTO `tn_comment` VALUES (2, 26, 55, 'asd', 1574234271);
INSERT INTO `tn_comment` VALUES (3, 26, 55, '哈哈哈哈哈哈', 1574234704);
INSERT INTO `tn_comment` VALUES (4, 26, 55, 'asdsad', 1574234857);
INSERT INTO `tn_comment` VALUES (5, 26, 55, 'sadsad', 1574234873);
INSERT INTO `tn_comment` VALUES (6, 26, 55, 'sad', 1574234948);
INSERT INTO `tn_comment` VALUES (7, 26, 55, 'asd', 1574248365);
INSERT INTO `tn_comment` VALUES (8, 26, 55, '还不', 1574248435);
INSERT INTO `tn_comment` VALUES (9, 26, 55, '你好啊', 1574248771);
INSERT INTO `tn_comment` VALUES (10, 26, 55, '你好啊', 1574248780);
INSERT INTO `tn_comment` VALUES (11, 26, 55, '哈哈哈哈哈哈哈', 1574248866);
INSERT INTO `tn_comment` VALUES (12, 26, 55, 'asd', 1574248928);
INSERT INTO `tn_comment` VALUES (13, 26, 55, 'sd', 1574249060);
INSERT INTO `tn_comment` VALUES (14, 26, 55, '人要努力', 1574249526);
INSERT INTO `tn_comment` VALUES (15, 26, 55, '为了过上更好的生活', 1574249627);
INSERT INTO `tn_comment` VALUES (16, 26, 55, 'asd', 1574249880);
INSERT INTO `tn_comment` VALUES (17, 26, 55, 'hahah', 1574249964);
INSERT INTO `tn_comment` VALUES (18, 26, 55, '我喜欢你', 1574249979);
INSERT INTO `tn_comment` VALUES (19, 26, 55, '\"会不会也在冬天的冷风里赶路，在地铁的那阵短暂停留中闭眼休息，在楼下的快餐厅买一份速食解决晚饭，在深夜里依旧不敢疲惫，努力守护着心中那抹光亮。', 1574250143);
INSERT INTO `tn_comment` VALUES (20, 26, 62, '还好哈哈哈', 1574819194);
INSERT INTO `tn_comment` VALUES (21, 26, 62, '不错', 1574819204);

-- ----------------------------
-- Table structure for tn_group
-- ----------------------------
DROP TABLE IF EXISTS `tn_group`;
CREATE TABLE `tn_group`  (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` int(10) NULL DEFAULT NULL,
  PRIMARY KEY (`group_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tn_group
-- ----------------------------
INSERT INTO `tn_group` VALUES (1, '超级管理员', 1587854384);
INSERT INTO `tn_group` VALUES (2, '默认用户组', 1568803859);

-- ----------------------------
-- Table structure for tn_group_rule
-- ----------------------------
DROP TABLE IF EXISTS `tn_group_rule`;
CREATE TABLE `tn_group_rule`  (
  `group_id` int(11) NOT NULL,
  `rule_id` int(11) NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tn_group_rule
-- ----------------------------
INSERT INTO `tn_group_rule` VALUES (1, 28);
INSERT INTO `tn_group_rule` VALUES (1, 15);
INSERT INTO `tn_group_rule` VALUES (1, 38);
INSERT INTO `tn_group_rule` VALUES (1, 4);
INSERT INTO `tn_group_rule` VALUES (1, 1);
INSERT INTO `tn_group_rule` VALUES (1, 31);
INSERT INTO `tn_group_rule` VALUES (1, 11);
INSERT INTO `tn_group_rule` VALUES (1, 16);
INSERT INTO `tn_group_rule` VALUES (1, 9);
INSERT INTO `tn_group_rule` VALUES (1, 23);
INSERT INTO `tn_group_rule` VALUES (1, 7);
INSERT INTO `tn_group_rule` VALUES (1, 12);
INSERT INTO `tn_group_rule` VALUES (1, 13);
INSERT INTO `tn_group_rule` VALUES (1, 14);
INSERT INTO `tn_group_rule` VALUES (1, 2);
INSERT INTO `tn_group_rule` VALUES (1, 8);
INSERT INTO `tn_group_rule` VALUES (1, 17);
INSERT INTO `tn_group_rule` VALUES (1, 18);
INSERT INTO `tn_group_rule` VALUES (1, 19);
INSERT INTO `tn_group_rule` VALUES (1, 20);
INSERT INTO `tn_group_rule` VALUES (1, 21);
INSERT INTO `tn_group_rule` VALUES (1, 22);
INSERT INTO `tn_group_rule` VALUES (1, 10);
INSERT INTO `tn_group_rule` VALUES (1, 24);
INSERT INTO `tn_group_rule` VALUES (1, 25);
INSERT INTO `tn_group_rule` VALUES (1, 26);
INSERT INTO `tn_group_rule` VALUES (1, 29);
INSERT INTO `tn_group_rule` VALUES (1, 5);
INSERT INTO `tn_group_rule` VALUES (1, 27);
INSERT INTO `tn_group_rule` VALUES (1, 30);
INSERT INTO `tn_group_rule` VALUES (1, 6);
INSERT INTO `tn_group_rule` VALUES (1, 32);
INSERT INTO `tn_group_rule` VALUES (1, 33);
INSERT INTO `tn_group_rule` VALUES (1, 34);
INSERT INTO `tn_group_rule` VALUES (1, 35);
INSERT INTO `tn_group_rule` VALUES (1, 36);
INSERT INTO `tn_group_rule` VALUES (1, 37);
INSERT INTO `tn_group_rule` VALUES (1, 3);
INSERT INTO `tn_group_rule` VALUES (1, 39);
INSERT INTO `tn_group_rule` VALUES (1, 40);
INSERT INTO `tn_group_rule` VALUES (1, 41);
INSERT INTO `tn_group_rule` VALUES (1, 42);
INSERT INTO `tn_group_rule` VALUES (1, 43);
INSERT INTO `tn_group_rule` VALUES (2, 7);
INSERT INTO `tn_group_rule` VALUES (2, 33);
INSERT INTO `tn_group_rule` VALUES (2, 34);
INSERT INTO `tn_group_rule` VALUES (2, 35);
INSERT INTO `tn_group_rule` VALUES (2, 36);
INSERT INTO `tn_group_rule` VALUES (2, 37);
INSERT INTO `tn_group_rule` VALUES (2, 38);
INSERT INTO `tn_group_rule` VALUES (2, 39);
INSERT INTO `tn_group_rule` VALUES (1, 44);
INSERT INTO `tn_group_rule` VALUES (1, 60);
INSERT INTO `tn_group_rule` VALUES (1, 61);
INSERT INTO `tn_group_rule` VALUES (1, 62);
INSERT INTO `tn_group_rule` VALUES (1, 63);
INSERT INTO `tn_group_rule` VALUES (1, 64);
INSERT INTO `tn_group_rule` VALUES (1, 65);

-- ----------------------------
-- Table structure for tn_link
-- ----------------------------
DROP TABLE IF EXISTS `tn_link`;
CREATE TABLE `tn_link`  (
  `link_id` int(11) NOT NULL AUTO_INCREMENT,
  `link_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `link_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type` int(10) NULL DEFAULT 0,
  `create_time` int(10) NOT NULL,
  PRIMARY KEY (`link_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tn_link
-- ----------------------------
INSERT INTO `tn_link` VALUES (1, '天年网络', 'https://www.tn721.cn', 0, 1597845682);
INSERT INTO `tn_link` VALUES (2, 'ThinkPHP', 'http://www.thinkphp.cn', 0, 1569659999);
INSERT INTO `tn_link` VALUES (3, 'TnCMS', 'https://www.tn721.cn', 1, 1571828997);
INSERT INTO `tn_link` VALUES (4, '企业官网', 'http://qy.tn721.cn', 2, 1571829059);
INSERT INTO `tn_link` VALUES (5, '官网发布', 'https://www.tn721.cn', 3, 1571829086);
INSERT INTO `tn_link` VALUES (6, '关于我们', 'https://www.tn721.cn', 4, 1571829188);
INSERT INTO `tn_link` VALUES (7, '联系我们', 'https://www.tn721.cn', 4, 1571829195);

-- ----------------------------
-- Table structure for tn_nav
-- ----------------------------
DROP TABLE IF EXISTS `tn_nav`;
CREATE TABLE `tn_nav`  (
  `nav_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `nav_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `order` int(255) NOT NULL DEFAULT 100,
  PRIMARY KEY (`nav_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tn_nav
-- ----------------------------
INSERT INTO `tn_nav` VALUES (1, 0, '下载', '', 4);
INSERT INTO `tn_nav` VALUES (2, 0, '演示', '', 3);
INSERT INTO `tn_nav` VALUES (3, 2, 'TnCMS', 'http://v6.tn721.cn', 100);
INSERT INTO `tn_nav` VALUES (4, 2, '企业官网', 'http://qy.tn721.cn', 100);
INSERT INTO `tn_nav` VALUES (5, 1, 'TnCMS', 'https://gitee.com/tn721/TnCMS', 100);
INSERT INTO `tn_nav` VALUES (6, 1, '微信小程序', 'https://gitee.com/tn721/tncms_wechat_applet', 100);
INSERT INTO `tn_nav` VALUES (9, 0, '<span style=\"color:red\">资讯中心</span>', '/news', 2);

-- ----------------------------
-- Table structure for tn_post
-- ----------------------------
DROP TABLE IF EXISTS `tn_post`;
CREATE TABLE `tn_post`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `status` int(2) NULL DEFAULT 0,
  `cate_id` int(11) NOT NULL,
  `cover_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `uid` int(11) NULL DEFAULT NULL,
  `read_count` int(255) NULL DEFAULT 0,
  `create_time` int(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 73 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tn_post
-- ----------------------------
INSERT INTO `tn_post` VALUES (42, '爱情是婚姻的坟墓？找对了人，只会是爱情的归宿。', '<p><br/><br/></p><p>有朋友跟我说:七大姑八大姨什么都好，就是过年时候老催自己找个对象结婚。没有对象，回家过年除了父母刨根问底的，一大帮亲戚也绝对不会手下留情。</p><p>为什么没有对象？为什么不谈对象？到底是自己眼光太高，还是别人眼光太高。各种各样的猜测到了最后，就是一帮亲戚们帮你安排相亲节目了。</p><p>没到谈婚论嫁的年纪时候，心里面很自信，觉得自己怎么可能去相亲，相信一定会遇到真心喜欢的女孩，在内心里期许一场美好浪漫的爱恋，一定可以走到一生一世，走到时间苍老，走到世界的尽头。现在到年纪了，还是没有对象，只能心里默默安慰自己，优秀的人都是留到最后的。</p><p>你去相亲了吗？我不去相亲，就在去相亲的路上。</p><p>父母让你去相亲，是想让你有一个幸福美好的家庭。但你希望婚姻是因为爱情，而不是因为将就生活。</p><p>一朋友跟我谈心时说:“我想结婚了，想和喜欢的人一起生活，养只狗，想清早起床睁眼第一个看的是他“。</p><p>陈小春演唱的『相依为命』中说:<br></p><p>“即使身边世事再毫无道理，与你永远亦连在一起，你不放下我，我不放下你，我想确定每日挽住同样的手臂，不敢早死要来陪住你，我已试够别离并不很凄美，我还如何撇下你”</p><p>有爱情的婚姻，两个人会时刻想着对方，就像歌词中说的:“你放不下我，我放不下你”。</p><p>两个人在一起最多的就是陪伴，我忙我的，你忙你的，有两三个小时的晚间黄金时间，我们都是独享的，但知道另一个空间里有另一个人坐在那儿，你就会感到很踏实，这就是爱情，最常态的爱情。</p><p>一个听得懂与聊得来的伴，会让你的生活不再枯涩，乍见之欢，不如久处不厌，所谓的合适，大概就是听得懂与聊得来。</p><p>网上看到的一位网友的爱情观:“我自认为爱情呢就是，在一起开心的时候很开心，伤心的时候很伤心，所有的相处都会从一开始的喜欢变成最后的随心所欲，电视剧里的爱情故事那只是让我们看了之后会相信爱情还是美好的，可要认清现实，总而言之，遇到喜欢的人就勇敢去追求，不喜欢的千万不能将就。”</p><p>李荣浩唱的『不将就』里说:\"你问我为什么顽固而专一，天下太大总有人比你更合适，其实我觉得这样不值，可没选择方式，你一出场别人都显得不过如此\"。</p><p>不将就不是挑剔，而是选择合适的人，心里喜欢的人，你才会觉得合适。</p><p>很多人结婚后不会对另一半说什么感人的话，也不会做什么感人的事，偶尔会说一些好听的话，也不知道有多少是真心的，有多少是随便说说，这些更多的关乎性格。</p><p>能永远在一起重要的是他是不是一个不错的人，越来越多的经历和成熟让人不太相信甜言蜜语，更务实了，但还是相信爱情。</p><p>爱情不是婚姻永远的坟墓，找对了人，只会是爱情的归宿。</p><p>最后，愿你嫁给爱情。</p><br/><br/>', 0, 2, '', 1, 0, 1574079199);
INSERT INTO `tn_post` VALUES (43, '我努力赚钱，真的仅仅只是因为要好好生活。', '<p><br/><br/></p><p>我很庆幸自己还很年轻，没有什么负担，可以尝试去做自己喜欢的事。</p><p><img src=\"/uploads/postImages/20191118/fdbacdac372d23bd206b21c26b146fba.jpg\" style=\"max-width:100%;\"></p><p>我身边的很多朋友，都已经结婚生子了，很羡慕他们有了自己幸福美满的生活。有一个高中同学，叫小江，高中时和我可谓是形影不离，但由于高考时没发挥好，没考上理想的学校。于是他就去外地工作了，工作了一年，找个了个对象，现在已经结婚了。</p><p>去年回老家，好久不见邀他来家里聚聚。我们侃侃而谈，我问他在外地工作是否顺利，这几年过得还好吗？他回答我说:“我很后悔当初放弃了念大学，现在只能在工厂车间里面做苦力活，很苦很累但薪水不多。”我问他:“你为什么不利用业余时间去学习你喜欢的东西，去发展自己的特长呢？”他回答说:“现在有了老婆和孩子，业余时间又少，一有时间就陪他们，有时候下班回来了很累，哪有心思去学习其他的东西。”我问他:“为何不换一个比较轻松的工作呢”他说:“像我这样没有什么特长的，轻松的工作薪水很少，养不活自己和家人，更别提买车子，房子了”</p><p>谁不想做个钱人呢？有人梦想买个彩票，中个几百万，然后不用辛辛苦苦的去工作。有人渴望自己像王思聪一样，一生下来就是富二代。也有人看清现实，勤勤恳恳的去打拼，去奋斗，想靠自己挣个几百万。</p><p>你说这些一心想着挣钱的人俗吗？一点都不去俗，他们也只是为了更好的生活。不出来生活试试，哪里知道柴米油盐贵。</p><p>一个同事问我，如果你银行卡里突然多了500万，你会拿去干嘛。我想了想，对他说:“可能会去旅游，带着相机去拍美丽的风景，看看各地的风土人情”，我反问他，他回答我说:“如果有了500万，我会带着自己的爱人，回到老家，自己种一些水果，开一家水果店，每周的一三五我守店，二四六她守店，周天休息或者一家人出去游玩，每天没有忧愁，没有烦恼，只顾卖好水果就行”。</p><p>没有了钱，生活中的柴米油盐都是烦恼，何谈生活呢，只不过是侥幸生存罢了。</p><p>很多人说90后赶上了最好的时代，但我认为这是需要具体去区分的，每个人都有每个人的境遇。</p><p>在大城市高昂的房价下，买房几乎成为了这些年轻人可望不可即的梦想。焦虑，迷茫，是年轻人生活中的常态。有千千万万的年轻人背井离乡来到大城市打拼的，他们满怀梦想来到人潮涌动的闹市街头，也见惯了凌晨三点的街道，却不能拥有一盏真正为自己而亮的灯光，大城市容不下肉身，小城市容不下灵魂。</p><p>王尔德曾经说过：我年轻时还以为金钱最重要，如今年纪大了，发现那句话一点不假。</p><p>在微博上看见那么一句话:“因为太穷，出来挣钱，辛苦地赚钱，不是因为多爱钱！ 而是这辈子， 不想因为钱和谁低三下四， 也不想因为钱而为难谁； 只希望在父母年老时， 可以有能力分担； 在孩子需要时， 不会囊中羞涩。 ”</p><p>没有钱， 你拿什么维持你的亲情， 稳固你的爱情，联络你的友情， 靠嘴说吗， 别闹了，大家都挺忙的！<br></p><p>谈起金钱可能会显得庸俗，但没有金钱，拿什么去谈情怀。</p><p>我们努力赚钱，不是因为爱钱。而是这辈子，不想因为钱和谁在一起，也不想因为钱而离开谁。</p><p>我努力赚钱，不是因为爱钱，真的仅仅只是因为要好好生活。</p><br/><br/>', 0, 4, '/uploads/postImages/20191118/fdbacdac372d23bd206b21c26b146fba.jpg', 1, 5, 1574079268);
INSERT INTO `tn_post` VALUES (44, '你为何宁愿吃生活的苦，却也不愿吃学习的苦？', '<p><br/><br/></p><p>逛知乎时看到这么一个问答：为什么大多数人宁愿吃生活的苦也不愿吃学习的苦？</p><p><img src=\"/uploads/postImages/20191118/0ca78726320121bd9ae02adc096b771b.jpg\" style=\"max-width:100%;\"></p><p>点赞最高的回答是：“大概是因为懒，学习的苦需要主动去吃，生活的苦，你躺着不动它就来了。”</p><p>一个刚从学校毕业出来的朋友，他跟我吐槽说：“生活好苦啊，好怀念以前学校的日子，不愁吃穿，没钱了就给家里打电话，钱就到位了。可现在就算没钱了也没有理由跟家里要了，出来社会了才知道柴米油盐贵”。他很后悔高中时没有用心去学习，没有考上一个好的大学。</p><p>人总是如此，常常察觉自己此刻的愚蠢，又不能接受自己下一刻还是如此愚蠢，还不去改变。</p><p>我是一个刚出来社会不久的学生，说一说自己的经历和感受。</p><p>我是一个大专文凭的，刚开始总是从别人口中听说什么学历不重要之类的一句话，自己也觉得学历确实不重要，重要的是自己的能力。但后来找工作的时候慢慢发现，学历也是必不可少的，至少它是限制进入一个公司的门槛。我遇到的有一个老板是这么说的：“学历不重要，重要的是能力，但你连考一个好的学历的能力都没有，我凭什么相信你有这个能力”。虽然说读书考大学的年纪是青春懵懂的年纪，还只是贪玩不思进取的年纪。你可以找借口说那时太贪玩了，如果从来一次，自己也能考上一个好的学校。</p><p>人要自信，但有时候不要太看得起自己，毒鸡汤醍醐灌顶的话谁不会说，能坚持去执行的能有几个。</p><p>大学是一个安逸的环境，它能让高中时的学霸变成学渣，但它也是个神奇的地方，能让高中时的学渣找到自己的方向和兴趣，逆袭成为学霸。</p><p>我大学的一个同学，据他说，高中时他是班上成绩最差的一位，所有坏学生的形象，在他的身上都能找到，成绩差，逃课，上课开小差，每天早上总是迟到那么几分钟，最糟糕的一次是他的班主任曾跟他的家长商量，叫他退学，但他的家长拒接了，所以现在成了我大学的同学。到了大学，他成了我们学院的学霸，我们学院几乎每个老师和学生都认识他。我们是计算机专业的，很喜欢这个专业。我们两关系还算一般，我也还算是个爱学习的人，经常跑他们宿舍去找他请教一些专业知识上的问题，也会偶尔去找他出去玩或者打游戏之类的，但每次去找他，发现他总是在研究专业上的技术知识，叫他一起打游戏或者出去玩，他总会因为专业技术上的问题拖个半小时一小时。</p><p>兴趣是最好的老师，在他的坚持和努力之下，他的专业知识强于其他很多的学生，参加了省内和国内的一些技术大赛，都获得了奖项。闪亮发光的金子总是格外的吸引人，就在大二结束的时候，他接受了企业和学校的安排，提前毕业出去工作了。他努力学习，吃了学习的苦，也得到了相应的回报。</p><p>大多数人总是抱怨生活苦，却从来不去改变，不去学习。</p><p>学习不只是在学校里，出来社会才只是人生的开始，社会之前的经历，只是热身运动而已。记得《爱情公寓》里关谷经常说的一句话：“活到老学到老”，只有努力学习，才能更好的活下去，落后就要挨打。</p><p>有人说，吃生活的苦，而不吃学习的苦，很多时候是因为没得选的，因为很多人，在一开始懵懂不识字时，就没能有幸地培养出学习的乐趣，但如果你吃了生活的苦，我相信学习的乐趣是不需要去培养的，因为生活会迫使你去学习。</p><p>有网友说:“学习的苦，犹如走独木桥过河，是一件精巧的事，走得太快不行，走得太慢不行，步子太大不行，步子太小也不行，总之是处处要用心，不用心可能就跌下桥去。”</p><p>喝了很多毒鸡汤，瞬间醍醐灌顶，但美好的生活单纯靠意念是行不通的。</p><p>十二国记中说：“我在这里学到了很多东西，其中最大的收获就是知道了自己多么愚蠢。不是我故作谦卑满足自己的心理，我真的很愚蠢，总是看着别人的脸色，没有真正的自我，但是，终于...我想去寻找不再愚蠢的自己，从现在开始，从现在开始一点点努力，然后能成为好一点的人就好了”。</p><p>“只有极其努力，才能看起来毫不费力”</p><p>“撑不住的时候，可以对自己说声“我好累”，但永远不要在心里承认说“我不行！”</p><p>只有吃尽学习的苦，你才不会被迫谋生。</p><br/><br/>', 0, 5, '/uploads/postImages/20191118/0ca78726320121bd9ae02adc096b771b.jpg', 1, 8, 1574079387);
INSERT INTO `tn_post` VALUES (45, '你要会发光，生活才会发光', '<p>文|毛毛虫小姐<br></p><div><div><p>玫瑰花总是有刺，即使是天使也会有瑕疵，我相信天上的星星要是伤了风也会坠落凡尘，太阳里总会看到黑子，最醇美的葡萄酒也有木桶的味道。</p><p><b>所以生命中必须有裂缝，阳光它才照射的进来。</b></p><p>人生就像抛物线，起起落落。无论a是否大于0，它都会存在一段单调递减的路程，但终会在一个极值点开始另一段单调递增的旅途。<b>这当中最重要的是漫长量变带来的质变。</b></p><p>狄更斯在《双城记》里写：这是一个最好的时代，这是一个最坏的时代；这是一个智慧的年代，这是一个愚蠢的年代；这是一个光明的季节，这是一个黑暗的季节；这是希望之春，这是失望之冬；我们拥有一切，我们一无所有；我们正踏上天堂之路，我们正走向地狱之门…</p><p><b>然而我想说无论在什么时代，什么季节，什么处境只要心中存有光亮，就会拥有崛起的力量。</b></p><p>每个人的心中都会存在光亮，只不过有些人光点大而亮，有些人光点小而暗，但无论是何种人，都会凭借心中的那抹光亮指引自己人生的走向。</p><p>每次回家都会路过一条深幽的小巷，这条深幽的小巷里，有几户紧挨着的人家，落了漆的小门，门上的铜铃不曾发过几声“咚咚”的回响。只有清晨的“吱呀”伴着泼出的水花，以及日落袅袅升起时的炊烟看似有人家。</p><p>然而就是这样一条幽深的小巷里，却独有一位老妪，经常久久的坐在一棵长得不甚茂盛的柳树下，柳树长在小巷尽头紧挨着河边一侧，茂盛的枝桠已经垂到了河里，柳树的风干的树皮看上去已经有了好些年头，看似和这位老妪长成了一般年岁。</p><p> “老妪—老妪”，不曾听过长辈们呼唤她的名字，只能唤她一声阿婆。这位阿婆不喜言辞，脸上却总是挂着笑容，脱落了的牙齿，凹陷的牙床使她看起来更加慈祥和蔼，爬满了皱纹的脸颊眼神中却能散发出光亮。</p><p>就是这样一位阿婆在那棵柳树下一天一月一年的静谧的坐着，远离了社会尘嚣，远离了高楼大厦，远离了世界种种烦恼，就这么带着笑容静谧的坐着，我想这位阿婆年轻时一定是一位有故事的姑娘，才会在长满华发的年纪里回忆自己的暮色年华。</p><p> 我不忍心打断她的回忆，但是好奇心却又驱使着我去剖析她丰富的内心世界，去了解她过往的生活与经历，究竟是什么让她面对生活如此安详，面对浮躁的环境又是如此静谧，我不知。</p><p>于是我想叩开她的心扉，徜徉在她的故事里，她过往的生命里，想探索在她心底的那一抹光亮，那一束阳光。</p><p><b>衣不如新，人不如故，那铺满青苔的石板路，那镶嵌着霉菌的老屋，那高耸的水塔以及忽明忽暗的煤油灯和电瓦斯是她儿时回忆的来路</b>。</p><p>那个是一个连温饱都难以解决的时代，阿婆没受过多少文化教育，没有那么多的思路，没有伟大的梦想，有的只有直面现实苦难的勇气。</p><p>为了生计，阿婆什么脏活累活都做，但是对生活从未服输妥协过，她说只要还有力气，就有大把的活可以去做，只要生活没有打垮我，日子总会一天过的比一天好，暮年的阿婆眼里仍然闪烁着光亮，这光是对生活的满怀希望，是对生命的无比热忱。</p><p>如今的阿婆在经历了生命中种种起伏以后，终于又回归了平静，她可以在朝霞升起的清晨里想想自己儿时的生活，在晚霞落幕里想想自己经历过的幸与不幸，然后安享晚年。</p><p><b>所以说生活与命运啊，终会对得起努力的人儿，在你不曾注意的瞬间里，它们总会以一种比较特别的方式给你回应。</b></p><p>你要有光，发自内心的光亮，它是你生活的信仰，那些在白天黑夜里无法跨越的挫折都会败在这光亮之下，如果你实在坚持不下去的时候，凭借这束光亮，你就会拥有崛起的力量。</p><p>多年以后当你老了，戴着老花镜，翻看自己日记的时候，一页一页满满的都是对自己的感谢，感谢这几十努力的自己，就让眼前年轻的生命充满荆棘吧，披荆斩棘以后才会通往另一个精彩的世界。</p></div><br>作者：毛毛虫小姐<br></div>', 0, 3, '', 1, 2, 1574079463);
INSERT INTO `tn_post` VALUES (46, '你看过许多美景，却仍说不出旅行的意义', '<p><img src=\"/uploads/postImages/20191118/11b96b7df9132f20d32cab7b6b8403e2.jpg\" style=\"max-width:100%;\"></p><p><br/><br/></p><div><div><p>一段旅途的开始，往往是另第一段人生故事的开始<br></p><p><b>01</b></p><p>我去过乌镇，那座带着水上人家最后枕上梦的古镇，如它的名字，小河从古镇穿过，桨撸声声，乌篷船行一阵停一阵，偶尔有几声吆喝从那头远远的传来，河风伴着阵阵青草味缓缓飘来，白墙青瓦里，住着好些静谧的人家，也有老人搬了竹椅靠着墙角闲话纳凉……</p><p>我去时正是初夏时节，去的早，行人不多。慢慢走过一条又一条青石板铺就的小路，路过一家又一家别致的小屋，却在一条悠悠的巷子口驻足停留，那个巷口静静的坐落着一家并不显眼的杂货铺，有一扇半打开着的窗户，窗口挂着一块小黑板，上面用白色粉笔写着：<b>旅行就是从你生活厌倦的地方到别人生活厌倦的地方，周而复始的去体验别人的生活。</b>便是这句话紧紧抓住了我前行的步伐。</p><p><br></p><div><br/><div><br/><br/><div><img125223\" src=\"//upload-images.jianshu.io/upload_images/7044932-0e46ddb7a1289783.jpg?imageMogr2/auto-orient/strip|imageView2/2/w/720/format/webp\"></img125223\"></div><br/></div><br/><div>乌镇的一家小店</div><br/></div><p><b>02</b><br></p><p>我开始在心里揣测，写出这样文字的杂货铺会拥有一位怎么样的主人，是高是矮，是胖是瘦，是带着眼镜穿着格子衬衫的文艺少年，还是饱经沧桑，历经生活艰苦的中年大叔。</p><p>我推门而进，一阵“叮叮当当”声传入，原是门上挂了一串铃铛。铺子不大，好似转个身，就能在空间视觉上把它看尽，但好似又看不尽，你细细盯住这一处又发现那一处的不同，每一处都是独到的风景，真可谓麻雀虽小，五脏俱全。</p><p>高高矮矮的架子上错落有致的摆满了物品，微微泛黄卷起一角的书籍、手工制造的茶杯，刻画着工匠独有的心思、带着温度还未被送至远方的明信片，各式各样的风铃挂满了整一个屋，河风吹来，清脆的在屋里回响“叮铃，叮铃”。</p><p>主人正埋头刻画着手里的葫芦丝，我们进去时，他没有抬头，只是声音低哑的说了句：随意看看。</p><p><b>03</b></p><p>出人意料的是这位主人是穿着格子衬衫的中年大叔，戴着眼镜，微胖，眯着眼，袖子被撸了上去，一只眼睛上还戴着放大镜，十分耐心又细心的搓捻着手里的葫芦丝。</p><p>我挑了很久，终于选定一串风铃，付钱时随口问起他是否是本地人，他笑笑说不是，我又问他来这里多久了，他说两年多了，还没问出第三个问题，他便开口到：“之前一直在北漂，厌倦了那种生活，有一次来这里旅游，喜欢上了这里宁静的生活，就在这里停留下来了，指不定过上两年还会走。”</p><p>“那你这家铺子走后要怎么办？”</p><p>“还会有下一个人来这里，爱上它，为它停留的！”</p><p>“然后再不断的离开吗”</p><p>“嗯，旅行就是从你生活厌倦的地方到别人生活厌倦的地方，周而复始的去看别人的生活”</p><p><b>04</b></p><p>他重复了那句话，我好像也听懂了这句话背后的意义，旅行就是不断走，不断停，去看不同的风景，去体验不同的人生活法。</p><p>林语堂在《生活的艺术》里写到：一个真正的旅行家必是一个流浪者，经历着流浪者的快乐、诱惑，和探险意念。旅行必须流浪式，否则便不成其为旅行。旅行的要点在于无责任、无定时、无来往信札、无嚅嚅好问的邻人、无来客和无目的地。一个好的旅行家决不知道他往那里去，更好的甚至不知道从何处而来。他甚至忘却了自己的姓名。</p><p><b>是啊，一个好的旅行者必是一个孤独的流浪者，只有流浪的人才能不管不顾明天在哪里，遇到什么人，碰到什么事，不用担心季节里草儿是否肯绿，花儿是否愿开，他关心的只是脚下的路途是否仍要继续。</b></p><p><br></p><p><b>05</b><br></p><p>在一成不变的生活夹缝中求生存，难免会幻想“在别处”的美好，那里有清新的空气和恬静的生活；有高薪的待遇和闲暇的时光，更有崭新的梦想和志同道合的人群。</p><p>我们习惯了时刻绷紧神经之弦待命，哪怕是周末也像是在与时间赛跑，两点约了朋友喝茶，一点就要出发，因为怕堵，五点必须吃完晚饭否则就赶不上六点半的电影开场。我们也厌倦这种枯燥的“三点一线”，以为在别处就可以摆脱所有的烦恼，这是现代人特有的心态，每个人都认为别处才有最完美的生活，但真的到了别处，才发现还是原来的这一处好。</p><p><b>06</b></p><p><b>在浮躁的城市生活久了，对于旅行总带着一种不能言说的情感，是对灵魂的放飞，是对身体的放松，是对未知世界的向往，如饕餮般蚕食自由与清晰的空气，是紧缺的精神食粮，然而我们对它赋予的高度期望总会让大多数人失望。</b></p><p>真正走上了旅途才发现一切都不是设想的那般完美，于是渐渐开始抱怨旅途太远，时间太长，住宿的酒店环境欠缺，服务的态度太差，相伴的人对旅途的规划出入太大，大家都走马观花似的拍完照片，再被埋没在拥挤的人海里匆匆赶往下一个景点，你甚至开始埋怨当初为何要这么轻易的踏上征程，你开始怀疑旅行的意义。</p><div><br/><div><br/><br/><div><img73110\" src=\"//upload-images.jianshu.io/upload_images/7044932-a0ceff9e64e91658.jpg?imageMogr2/auto-orient/strip|imageView2/2/w/719/format/webp\"></img73110\"></div><br/></div><br/><div>一路走走停停</div><br/></div><p><b>07</b><br></p><p><b>但是我还是想告诉你啊，旅行的美不在于目的地，而在于沿途的风景，是欣赏风景的心情，是在旅途路中体验到的别人各式各样的生活。</b></p><p>在别人的生活中学就如何豁达的看待人事；旅途中遇到志同道合的人是种幸运，遇不到也是缘分，要学会用一种淡然的态度去面对世事，用一颗真诚的心去对待他人。</p><p>你必要去上一个地方，交上三俩好友，煮一杯茶，摆上三两壶酒，敞开心扉，诉说诉说彼此的故事，交换交换彼此的生活，才不辜负这大好时光。</p><p>所有准备旅行，或者正在旅行亦或者已经旅行好久好久的朋友，但愿你们能在看过的风景里学会一点两点生活的真谛，不是匆匆的向前奔去，而是不急不缓的沉浸在美景里，去体验各式不一的生活。</p><p><b>当你归来时，带回的不仅仅是相机里定格的回忆，还有无数鲜明的故事和别样的生活，而这些深深浅浅的经历不断累积成为你的人生阅历，所有走过的路都是人生指南，所有听过的长长短短的故事都是旅行的意义。</b></p><p>要去就去吧，不要犹犹豫豫，一如当初你义无反顾奔去时那样，勇敢上路！</p></div><br>作者：毛毛虫小姐<br></div><br/><br/>', 0, 3, '/uploads/postImages/20191118/11b96b7df9132f20d32cab7b6b8403e2.jpg', 1, 12, 1574079624);

-- ----------------------------
-- Table structure for tn_post_collection
-- ----------------------------
DROP TABLE IF EXISTS `tn_post_collection`;
CREATE TABLE `tn_post_collection`  (
  `uid` int(11) NOT NULL,
  `post_id` int(11) NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tn_post_collection
-- ----------------------------
INSERT INTO `tn_post_collection` VALUES (21, 29);
INSERT INTO `tn_post_collection` VALUES (21, 30);

-- ----------------------------
-- Table structure for tn_rule
-- ----------------------------
DROP TABLE IF EXISTS `tn_rule`;
CREATE TABLE `tn_rule`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(2) NULL DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `menu` int(2) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 66 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tn_rule
-- ----------------------------
INSERT INTO `tn_rule` VALUES (1, 0, '&#xe66f;', '', '会员管理', 0);
INSERT INTO `tn_rule` VALUES (2, 0, '&#xe655;', '', '内容管理', 0);
INSERT INTO `tn_rule` VALUES (3, 0, '&#xe673;', '', '权限管理', 0);
INSERT INTO `tn_rule` VALUES (4, 0, '&#xe716;', '', '系统设置', 0);
INSERT INTO `tn_rule` VALUES (5, 0, '&#xe656;', '', '主题插件', 0);
INSERT INTO `tn_rule` VALUES (6, 0, '&#xe64c;', '', '链接管理', 0);
INSERT INTO `tn_rule` VALUES (7, 0, NULL, '', '【前端】用户权限（以下权限为用户权限，不建议更改）', 1);
INSERT INTO `tn_rule` VALUES (8, 0, NULL, '', '其他权限', 1);
INSERT INTO `tn_rule` VALUES (9, 4, NULL, 'admin/system/customCode', '自定义代码', 0);
INSERT INTO `tn_rule` VALUES (10, 4, NULL, 'admin/system/Basics', '基本设置', 0);
INSERT INTO `tn_rule` VALUES (11, 1, NULL, 'admin/user/delGroup', '删除用户组', 1);
INSERT INTO `tn_rule` VALUES (12, 1, NULL, 'admin/user/addRole', '添加角色', 1);
INSERT INTO `tn_rule` VALUES (13, 1, NULL, 'admin/user/groupRule', '用户组权限详情', 1);
INSERT INTO `tn_rule` VALUES (14, 2, NULL, 'admin/post/postList', '文章列表', 0);
INSERT INTO `tn_rule` VALUES (15, 1, NULL, 'admin/user/userList', '会员列表', 0);
INSERT INTO `tn_rule` VALUES (16, 1, NULL, 'admin/user/saveRule', '修改权限', 1);
INSERT INTO `tn_rule` VALUES (17, 1, NULL, 'admin/user/userAdd', '添加用户', 1);
INSERT INTO `tn_rule` VALUES (18, 1, NULL, 'admin/user/delUser', '删除用户', 1);
INSERT INTO `tn_rule` VALUES (19, 2, NULL, 'admin/post/postAdd', '发布文章', 0);
INSERT INTO `tn_rule` VALUES (20, 2, NULL, 'admin/index/upload', '上传图片', 1);
INSERT INTO `tn_rule` VALUES (21, 1, NULL, 'admin/user/userStatus', '修改用户状态', 1);
INSERT INTO `tn_rule` VALUES (22, 8, NULL, 'admin/index/welcome', '用户欢迎页', 1);
INSERT INTO `tn_rule` VALUES (23, 2, NULL, 'admin/post/category', '文章分类', 0);
INSERT INTO `tn_rule` VALUES (24, 2, NULL, 'admin/post/delPost', '删除文章', 1);
INSERT INTO `tn_rule` VALUES (25, 4, NULL, 'admin/system/topArticle', '修改置顶推荐', 1);
INSERT INTO `tn_rule` VALUES (26, 4, NULL, 'admin/system/carousel', '修改轮播图配置', 1);
INSERT INTO `tn_rule` VALUES (27, 6, NULL, 'admin/system/editLink', '修改友情链接', 1);
INSERT INTO `tn_rule` VALUES (28, 6, NULL, 'admin/system/addLink', '添加友情链接', 1);
INSERT INTO `tn_rule` VALUES (29, 6, NULL, 'admin/system/deleteLink', '删除友情链接', 1);
INSERT INTO `tn_rule` VALUES (30, 4, NULL, 'admin/system/deleteNav', '删除导航', 1);
INSERT INTO `tn_rule` VALUES (31, 4, NULL, 'admin/system/saveNav', '修改导航', 1);
INSERT INTO `tn_rule` VALUES (32, 3, NULL, 'admin/user/adminRole', '角色管理', 0);
INSERT INTO `tn_rule` VALUES (33, 7, NULL, 'home/member/index', '个人中心', 1);
INSERT INTO `tn_rule` VALUES (34, 7, NULL, 'home/post/postAdd', '投稿文章', 1);
INSERT INTO `tn_rule` VALUES (35, 7, NULL, 'home/post/delPost', '删除文章', 1);
INSERT INTO `tn_rule` VALUES (36, 7, NULL, 'home/post/cancelCollection', '取消收藏文章', 1);
INSERT INTO `tn_rule` VALUES (37, 7, NULL, 'home/member/updateUser', '修改资料', 1);
INSERT INTO `tn_rule` VALUES (38, 7, NULL, 'home/member/upload', '修改头像', 1);
INSERT INTO `tn_rule` VALUES (39, 7, NULL, 'home/member/rePassword', '修改密码', 1);
INSERT INTO `tn_rule` VALUES (40, 5, NULL, 'admin/theme/index', '主题管理', 0);
INSERT INTO `tn_rule` VALUES (41, 6, NULL, 'admin/link/index', '链接列表', 0);
INSERT INTO `tn_rule` VALUES (42, 4, NULL, 'admin/system/smtp', '邮箱配置', 0);
INSERT INTO `tn_rule` VALUES (43, 4, NULL, 'admin/system/site', '网站SEO', 0);
INSERT INTO `tn_rule` VALUES (44, 4, NULL, 'admin/system/addNav', '添加友情链接', 1);
INSERT INTO `tn_rule` VALUES (60, 2, NULL, 'admin/post/cateAdd', '添加文章分类', 1);
INSERT INTO `tn_rule` VALUES (61, 2, NULL, 'admin/post/delCate', '删除文章分类', 1);
INSERT INTO `tn_rule` VALUES (62, 2, NULL, 'admin/post/updateCate', '修改文章分类', 1);
INSERT INTO `tn_rule` VALUES (63, 2, NULL, 'admin/post/upload', '文章分类封面上传', 1);
INSERT INTO `tn_rule` VALUES (64, 0, '&#xe677;', ' ', '微信小程序', 0);
INSERT INTO `tn_rule` VALUES (65, 64, NULL, 'admin/miniapp/baseSet', '基本设置', 0);

-- ----------------------------
-- Table structure for tn_system
-- ----------------------------
DROP TABLE IF EXISTS `tn_system`;
CREATE TABLE `tn_system`  (
  `config` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `extend` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `intro` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`config`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tn_system
-- ----------------------------
INSERT INTO `tn_system` VALUES ('carousel', '44,46', NULL, NULL);
INSERT INTO `tn_system` VALUES ('miniapp', '', '', NULL);
INSERT INTO `tn_system` VALUES ('siteInfo', '{\"title\":\"\\u707c\\u89c1\\u9752\\u5e74\\u7f51\",\"subtitle\":\"\\u6bcf\\u4e00\\u4e2a\\u9752\\u5e74\\u90fd\\u5e94\\u8be5\\u6709\\u81ea\\u5df1\\u72ec\\u7279\\u7684\\u601d\\u60f3\",\"keyword\":\"\\u6bcf\\u4e00\\u4e2a\\u9752\\u5e74\\u90fd\\u5e94\\u8be5\\u6709\\u81ea\\u5df1\\u72ec\\u7279\\u7684\\u601d\\u60f3\",\"description\":\"\\u6bcf\\u4e00\\u4e2a\\u9752\\u5e74\\u90fd\\u5e94\\u8be5\\u6709\\u81ea\\u5df1\\u72ec\\u7279\\u7684\\u601d\\u60f3\",\"copyright\":\"Copyright \\u00a9 2019 \\u707c\\u89c1\\u9752\\u5e74\\u7f51.\\u4fdd\\u7559\\u6240\\u6709\\u6743\\u5229  \\u6ec7ICP\\u590717000713\\u53f7\"}', NULL, NULL);
INSERT INTO `tn_system` VALUES ('smtp', 'mmteen@sina.com', 'mmteen@sina.com', '587');
INSERT INTO `tn_system` VALUES ('statistics', '<script>\nvar _hmt = _hmt || [];\n(function() {\n  var hm = document.createElement(\"script\");\n  hm.src = \"https://hm.baidu.com/hm.js?c3af965d263e887dee1beb8c37bde11f\";\n  var s = document.getElementsByTagName(\"script\")[0]; \n  s.parentNode.insertBefore(hm, s);\n})();\n\n//百度自动提交收录代码\n(function(){\n    var bp = document.createElement(\'script\');\n    var curProtocol = window.location.protocol.split(\':\')[0];\n    if (curProtocol === \'https\') {\n        bp.src = \'https://zz.bdstatic.com/linksubmit/push.js\';\n    }\n    else {\n        bp.src = \'http://push.zhanzhang.baidu.com/push.js\';\n    }\n    var s = document.getElementsByTagName(\"script\")[0];\n    s.parentNode.insertBefore(bp, s);\n})();\n\n//360自动提交收录代码\n(function(){\nvar src = \"https://jspassport.ssl.qhimg.com/11.0.1.js?d182b3f28525f2db83acfaaf6e696dba\";\ndocument.write(\'<script src=\"\' + src + \'\" id=\"sozz\"><\\/script>\');\n})();\n</script>\n', NULL, NULL);
INSERT INTO `tn_system` VALUES ('theme', 'default', 'amaze', NULL);
INSERT INTO `tn_system` VALUES ('topArticle', ' 43,44,45,46', NULL, NULL);

-- ----------------------------
-- Table structure for tn_user
-- ----------------------------
DROP TABLE IF EXISTS `tn_user`;
CREATE TABLE `tn_user`  (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `group_id` int(11) NOT NULL,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `openid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` int(10) NOT NULL DEFAULT 0,
  `intro` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `integral` int(255) NULL DEFAULT 0,
  `user_type` int(255) NOT NULL DEFAULT 0,
  `create_time` int(10) NULL DEFAULT NULL,
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tn_user
-- ----------------------------
INSERT INTO `tn_user` VALUES (1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'admin@admin.com', 1, '/uploads/avatar/20191119/4a83f55f1fc0d6ffdc64b3e9d3054e88.jpg', NULL, 0, '', 0, 0, NULL);

SET FOREIGN_KEY_CHECKS = 1;
