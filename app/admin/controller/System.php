<?php

namespace app\admin\controller;

use think\facade\Db;
use app\common\model\Link;
class System extends Base
{

    public function smtp()
    {
        if(request()->isAjax()){
            $data = input("post.");
            $updateSMTP = Db::name("system")->where("config","smtp")->update($data);
            if($updateSMTP){
                return json(["code" => 1 ,"msg" => "保存成功"]);
            }
            return json(["code" => 0 ,"msg" => "保存失败"]);
        }else{
            $smtp = Db::name("system")->where("config","smtp")->find();
            return view("/smtp",["smtp" => $smtp]);
        }
    }

    public function site()
    {
        if(request()->isAjax()){

            $data['title'] = input("title");
            $data['subtitle'] = input("subtitle");
            $data['keyword'] = input("keyword");
            $data['description'] = input("description");
            $data['copyright'] = input("copyright");

            $siteDataJson = json_encode($data);

            $result = Db::name('system')->where("config","siteInfo")->update(["value" =>$siteDataJson]);
            if($result){
                return json(["code" => 1 , "msg" => "修改成功"]);
            }
            return json(["code" => 0 , "msg" => "修改失败"]);

        }else{
            $siteInfo = Db::name("system")->where("config","siteInfo")->find();
            return view('/site',["siteInfo" => json_decode($siteInfo["value"],true)]);
        }
    }
    public function Basics()
    {
        //菜单导航
        $data=Db::name('nav')->order("order desc")->select();
        $navList = array();
        foreach ($data as $item){
            $navList[$item["parent_id"]][] = $item;
        }
        //友情链接
        $link = Link::select();
        //置顶文章配置
        $topArticle = Db::name("system")->where("config","topArticle")->find();
        //轮播图文章
        $carousel = Db::name("system")->where("config","carousel")->find();
        return view("/basics",[
            "navList" => $navList,
            "link" => $link,
            "topArticle" => $topArticle,
            "carousel" =>$carousel,
        ]);
    }

    public function customCode()
    {
        if (request()->isAjax()){
            $statistics = input("statistics");
            $update = Db::name("system")->where("config","statistics")->update(["value" =>$statistics]);
            if ($update){
                return json(["code" =>1,"msg" => "保存成功"]);
            }
            return json(["code" =>0,"msg" => "保存失败"]);
        }else{
            $statistics = Db::name("system")->where("config","statistics")->value("value");
            return view("/statistics",["statistics" => $statistics]);
        }
    }

    public function addNav()
    {
        $data["parent_id"] = input("navParent");
        $data["nav_title"] = input("navTitle");
        $data["url"] = input("navUrl");
        if(input("navTitle") == ""){
            return json(["code" => 0, "msg" =>"导航标题为空"]);
        }
        if( $data["parent_id"] == ""){
            $data["parent_id"] = 0;
        }

        $insert = Db::name("nav")->insert($data);

        if($insert){
            return json(["code" =>1 ,"msg" => "添加成功"]);
        }
        return json(["code" =>0 ,"msg" => "添加失败"]);
    }

    public function deleteNav()
    {
        $navId = input("navId");
        $navInfo = Db::name("nav")->where("nav_id",$navId)->find();

        //当是父级导航时删除父级导航及子导航
        if($navInfo["parent_id"] == 0){
            $isSubgrade = Db::name("nav")->where("parent_id",$navInfo["nav_id"])->find();
            if($isSubgrade){
                $deleteSub = Db::name("nav")->where("parent_id",$navInfo["nav_id"])->delete();
                $deleteParent = Db::name("nav")->where("nav_id",$navInfo["nav_id"])->delete();

                if( $deleteSub && $deleteParent){
                    return json(["code" => 1,"msg" => "删除成功"]);
                }
                return json(["code" => 0,"msg" => "删除失败"]);
            }
            $delete = Db::name("nav")->where("nav_id",$navId)->delete();
            if($delete){
                return json(["code" => 1,"msg" => "删除成功"]);
            }
            return json(["code" => 0,"msg" => "删除失败"]);
        }

        //子导航时直接删除
        $delete = Db::name("nav")->where("nav_id",$navId)->delete();
        if($delete){
            return json(["code" => 1,"msg" => "删除成功"]);
        }
        return json(["code" => 0,"msg" => "删除失败"]);
    }
    public function saveNav()
    {
        $data = input("post.");
        if($data["nav_title"] == ""){
            return json(["code" => 0, "msg" =>"导航标题为空"]);
        }
        $update = Db::name("nav")->where("nav_id",$data["nav_id"])->update($data);
        if($update){
            return json(["code" => 1,"msg" => "保存成功"]);
        }
        return json(["code" => 0,"msg" => "保存失败"]);
    }

    //置顶文章设置
    public function topArticle()
    {
        $data = input("post.");
        $update = Db::name("system")->where("config","topArticle")->update(["value" =>$data["value_"]]);
        if($update){
            return json(["code" => 1, "msg" => "保存成功"]);
        }
        return json(["code" => 0, "msg" => "保存失败"]);
    }

    //轮播图文章设置
    public function carousel()
    {
        $data = input("post.");
        $update = Db::name("system")->where("config","carousel")->update($data);
        if($update){
            return json(["code" => 1, "msg" => "保存成功"]);
        }
        return json(["code" => 0, "msg" => "保存失败"]);
    }

    public function deleteLink()
    {
        $linkId = input("linkId");
        $deleteLink = Db::name("link")->where("link_id",$linkId)->delete();
        if($deleteLink){
            return json(["code" => 1, "msg" =>"删除成功"]);
        }else{
            return json(["code" => 0, "msg" =>"删除失败"]);
        }
    }

    public function editLink()
    {

        $linkId = input("linkId");
        $linkTitle = input("linkTitle");
        $linkUrl = input("linkUrl");

        $data["link_title"] = $linkTitle;
        $data["link_url"] = $linkUrl;
        $update = Db::name("link")->where("link_id",$linkId)->update($data);
        if($update){
            return json(["code" => 1, "msg" =>"修改成功"]);
        }else{
            return json(["code" => 0, "msg" =>"修改失败"]);
        }
    }

    public function addLink()
    {

        $linkTitle = input("linkTitle");
        $linkUrl = input("linkUrl");
        $data["link_title"] = $linkTitle;
        $data["link_url"] = $linkUrl;
        $data["create_time"] = time();
        if($linkTitle == "" || $linkUrl == ""){
            return json(["code" => 0, "msg" =>"链接标题或地址为空"]);
        }
        $linkId = Db::name("link")->insertGetId($data);
        if($linkId){
            return json(["code" => 1,"data" =>['id'=>$linkId,'title' => $linkTitle,'url' => $linkUrl], "msg" =>"添加成功"]);
        }else{
            return json(["code" => 0, "msg" =>"添加失败"]);
        }
    }

    //统计代码
    public function statistics()
    {
        if (request()->isAjax()){
            $statistics = input("statistics");
            $update = Db::name("system")->where("config","statistics")->update(["value" =>$statistics]);
            if ($update){
                return json(["code" =>1,"msg" => "保存成功"]);
            }
            return json(["code" =>0,"msg" => "保存失败"]);
        }else{
            $statistics = Db::name("system")->where("config","statistics")->value("value");
            return view("/statistics",["statistics" => $statistics]);
        }

    }
}