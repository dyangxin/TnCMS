<?php


namespace app\admin\controller;


use app\common\model\GoodsCate;
use app\common\model\Order as orderModel;
use think\facade\Db;
use think\facade\View;
use app\common\model\Goods;
use  app\common\validate\Goods as GoodsValidate;
use think\exception\ValidateException;
class Shop extends Base
{

    public function order()
    {
        $orderList = Db::name("order")->order("id desc")->paginate(10,false,['query'=>request()->param()])->each(function($item, $key){
            $goodsId = $item["goods_id"];
            $goodsTitle = Db::name('goods')->where("id",$goodsId)->value("title");
            $item['goods_title'] = $goodsTitle;
            $item['create_time'] = date("Y-m-d", $item['create_time']);
            return $item;
        });

        View::assign("orderList",$orderList);
        return View::fetch("/order");
    }

    public function goods()
    {
        $goodList = Db::name("goods")->order("id desc")->paginate(10,false,['query'=>request()->param()])->each(function($item, $key){
            $cateId = $item["cate_id"];
            $cateName = Db::name('goods_cate')->where("id",$cateId)->value("cate_name");
            $item['cate_name'] = $cateName;
            $item['create_time'] = date("Y-m-d", $item['create_time']);
            return $item;
        });
        View::assign("goodList",$goodList);
        return View::fetch("/goods");
    }

    public function goodsAdd()
    {
        $id = input("id");
        if (request()->isAjax()){
            $data = input("post.");
            $str="Line1\nLine2\rLine3\r\nLine4\n";
            $order=array("\r\n","\n","\r");
            $replace='<br/>';
            $data["introduce"]=str_replace($order,$replace,$data["introduce"]);

            try {
                validate(GoodsValidate::class)->check($data);
            } catch (ValidateException $e) {
                // 验证失败 输出错误信息
                return json(["code" => 0,"msg" =>$e->getError()]);
            }

            if (empty($id)){
                preg_match('/<img.+src=\"?(.+\.(jpg|gif|bmp|bnp|png))\"?.+>/i',input("introduce","","trim"), $ereg);//正则表达式把图片的SRC获取出来了
                if(empty($ereg)) {
                    $cover_image = "";
                }else{
                    $cover_image = $ereg[1];//图片SRC
                }
                $data["image_cover"] = $cover_image;
                $data["uid"] = session("AdminId");
                $create = Goods::create($data);
                if ($create){
                    return json(["code" => 1,"msg" =>"发布成功"]);
                }
                return json(["code" => 0,"msg" =>"发布失败"]);
            }else if ($id > 0){
                $update = Goods::update($data);
                if ($update){
                    return json(["code" => 1,"msg" =>"修改成功"]);
                }
                return json(["code" => 0,"msg" =>"修改失败"]);
            }
        }else{
            $id = input("id");
            $goodsDetail = Goods::find($id);
            $category = Db::name("goods_cate")->select();
            View::assign("goodsDetail",$goodsDetail);
            View::assign("category",$category);
            return View::fetch("/goods-add");
        }
    }

    public function delGoods()
    {
        $id = input("id");
        $rel = Goods::destroy($id);

        if ($rel){
            return json(["code" => 1 ,"msg" =>"删除成功"]);
        }
        return json(["code" => 0 ,"msg" =>"删除失败"]);
    }
    public function delOrder()
    {
        $id = input("id");
        $rel = orderModel::destroy($id);

        if ($rel){
            return json(["code" => 1 ,"msg" =>"删除成功"]);
        }
        return json(["code" => 0 ,"msg" =>"删除失败"]);
    }

    public function goodsStatus()
    {
        $status = input("status");
        $id = input("id");
        $rel = Goods::update(["status" => $status],["id" => $id]);
        if ($rel){
            return json(["code" => 1,"msg" => "状态修改成功"]);
        }
        return json(["code" => 0,"msg" => "状态修改失败"]);
    }

    public function goodsCate()
    {

        $list = Db::name("goods_cate")->select();

        View::assign("list",$list);
        return View::fetch("/goods-cate");
    }

    public function delCate()
    {
        $id = input("cate_id");
        $del = GoodsCate::destroy($id);
        if ($del){
            return json(["code" => 1,"msg" =>"删除成功"]);
        }
        return json(["code" => 0,"msg" =>"删除失败"]);
    }

    public function cateAdd()
    {
        $cateName = input("cateName");
        if (empty($cateName)){
            return json(["code" => 0,"msg" =>"分类名不能为空"]);
        }
        $rel = GoodsCate::create(["cate_name" => $cateName]);
        if ($rel){
            return json(["code" => 1,"msg" =>"添加成功"]);
        }
        return json(["code" => 0,"msg" =>"添加失败"]);
    }

    public function updateCate()
    {
        $cateId = input("cate_id");
        if (request()->isAjax()){
            $cateName = input("cate_name");
            $rel = GoodsCate::update(["cate_name" => $cateName],["id" =>$cateId]);
            if ($rel){
                return json(["code" => 1,"msg" =>"修改成功"]);
            }
            return json(["code" => 0,"msg" =>"修改失败"]);
        }else{
            $cateDetal = Db::name("goods_cate")->where("id",$cateId)->find();
            View::assign("cateDetail",$cateDetal);
            return View::fetch("/goods-cate-update");
        }
    }
}