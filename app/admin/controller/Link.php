<?php

namespace app\admin\controller;

use think\facade\Db;
use think\facade\View;
use app\common\model\Link as LinkModel;
class Link
{
    //友情链接
    public function index()
    {
        $link = LinkModel::select();
        View::assign("Link",$link);
        return View::fetch("/Link");
    }

    public function deleteLink()
    {
        $linkId = input("linkId");
        $deleteLink = Db::name("link")->where("link_id",$linkId)->delete();
        if($deleteLink){
            return json(["code" => 1, "msg" =>"删除成功"]);
        }else{
            return json(["code" => 0, "msg" =>"删除失败"]);
        }
    }

    public function editLink()
    {

        $linkId = input("linkId");
        $linkTitle = input("linkTitle");
        $linkUrl = input("linkUrl");
        $linkType = input("type");

        $data["link_title"] = $linkTitle;
        $data["link_url"] = $linkUrl;
        $data["type"] = $linkType;
        $update = Db::name("link")->where("link_id",$linkId)->update($data);
        if($update){
            return json(["code" => 1, "msg" =>"修改成功"]);
        }else{
            return json(["code" => 0, "msg" =>"修改失败"]);
        }
    }

    public function addLink()
    {

        $linkTitle = input("linkTitle");
        $linkUrl = input("linkUrl");
        $linkType = input("type");

        $data["link_title"] = $linkTitle;
        $data["link_url"] = $linkUrl;
        $data["create_time"] = time();
        $data["type"] = $linkType;
        if($linkTitle == "" || $linkUrl == ""){
            return json(["code" => 0, "msg" =>"链接标题或地址为空"]);
        }
        $linkId = Db::name("link")->insertGetId($data);
        if($linkId){
            return json(["code" => 1,"data" =>['id'=>$linkId,'title' => $linkTitle,'url' => $linkUrl], "msg" =>"添加成功"]);
        }else{
            return json(["code" => 0, "msg" =>"添加失败"]);
        }
    }
}