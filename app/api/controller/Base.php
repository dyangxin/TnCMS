<?php


namespace app\api\controller;

use think\facade\Db;

class Base
{
    public function getUid($sessionKey)
    {
        $UID = Db::name("user")->where("openid",cache($sessionKey))->value("uid");
        return $UID;
    }
}