<?php
namespace app\common\model;


use think\Model;

class Post extends Model
{
    public function getStatusAttr($value)
    {
        $status = [0=>'公开',1=>'私密'];
        return $status[$value];
    }
    public function userInfo()
    {
        return $this->hasOne("User","uid","uid");
    }

    public function cate()
    {
        return $this->hasOne("Category","cate_id","cate_id");
    }

}