<?php

namespace app\common\model;


use think\Model;

class User extends Model
{
    protected $pk = 'uid';
    protected $autoWriteTimestamp = true;
    protected $updateTime = false;
    public function group()
    {
        return $this->hasOne("Group","group_id","group_id");
    }

    public function getUserTypeAttr($value)
    {
        $status = [0=>'账号密码',1=>'微信公众号',2=>'微信小程序'];
        return $status[$value];
    }
}