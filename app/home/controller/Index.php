<?php

namespace app\home\controller;

use app\common\model\Post as P;
use think\facade\Db;
class Index extends Base
{
    public function index()
    {

        //置顶文章
        $configTopArticle = Db::name("system")->where("config","topArticle")->value("value");
        $topArticle = P::withJoin(['userInfo'	=>	['username','uid'],'cate' =>['cate_name']])->where("id","in",$configTopArticle)->limit(4)->select();

        //轮播图文章
        $carouselConfig = Db::name("system")->where("config","carousel")->value("value");
        $carouselArticle = Db::name("post")->field("id,title,cover_image")->where("id","in", $carouselConfig)->select();

        //最新资讯
        $newsList = P::withJoin(['userInfo'	=>	['username','uid'],'cate' =>['cate_name']])->order("id desc")->limit(20)->select();
        return view('/index',[
            'topArticle'=>$topArticle,
            'carousel' => $carouselArticle,
            'newsList' => $newsList,
        ]);
    }
}