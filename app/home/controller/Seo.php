<?php


namespace app\home\controller;


class Seo
{

    public function pushBaiDu()
    {
        $TOKEN = "itnfa6gm6GNj9VcD";
        $URL = input("url");
        if (empty($URL)){
            return json(["code" =>0 ,"msg" => "URL为空"]);
        }
       if ($this ->checkURL($URL) == false){
           return json(["code" =>0 ,"msg" => "URL已收录，请勿重复提交"]);
       };
        $urls[] = $URL;
        $api = 'http://data.zz.baidu.com/urls?site='.request()->domain() .'&token=' .$TOKEN;
        $ch = curl_init();
        $options =  array(
            CURLOPT_URL => $api,
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS => implode("\n", $urls),
            CURLOPT_HTTPHEADER => array('Content-Type: text/plain'),
        );
        curl_setopt_array($ch, $options);
        $result = json_decode(curl_exec($ch));

        return json($result);
    }


    function checkURL($url)
    {
        $url = 'http://www.baidu.com/s?wd=' . urlencode($url);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $content= curl_exec($curl);
        curl_close($curl);
        if (!strpos($content, '没有找到')) { //没有找到说明已被百度收录
            return false;
        } else {
            return true;
        }
    }
}